const express = require("express");
const request = require("request");
const router = express.Router();
const UserController = require("../controllers/user");

router.post("/email-exists", (req, res) => {});

router.get("/details", (req, res) => {});

router.post("/", (req, res) => {
  UserController.register(req.body).then((result) => res.send(result));
});

router.get("/notify", (req, res) => {
  const shortCode = "21584514";
  const accessToken = "Hv64BcLZYViWJEFU37DizriqwWJDIMIHPQxl2wFfBZ4";
  const clientCorrelator = "123456";
  const mobileNumber = "9150378333";
  const message = "This is a test message from Sir Marty's Training";

  const options = {
    method: "POST",
    url:
      "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/" +
      shortCode +
      "/requests",
    qs: { 'access_token': accessToken },
    headers: { "Content-Type": "application/json" },
    body: {
      outboundSMSMessageRequest: {
        'clientCorrelator': clientCorrelator,
        'senderAddress': shortCode,
        'outboundSMSMessageRequest': { 'message': message },
        'address': mobileNumber
      },
    },
    json: true,
  };
  request(options, (error, response, body) => {
    if (error) throw new Error(error);
    console.log(body);
  });
});

module.exports = router;
