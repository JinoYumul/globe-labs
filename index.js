const express = require("express");
const app = express();
const mongoose = require("mongoose");
const { ApolloServer } = require("apollo-server-express");

mongoose.connection.once("open", () => {
  console.log("Connected to MongoDB");
});

//online connection
mongoose.connect(
  "mongodb+srv://jino:jino123@cluster1-7j4kj.mongodb.net/users?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

new ApolloServer({
  typeDefs: require("./graphql/schema"),
  resolvers: require("./graphql/resolvers"),
}).applyMiddleware({ app, path: "/graphql" });

app.use(express.json()); //instead of having bodyparser use this
app.use(express.urlencoded({ extended: true }));

const userRoutes = require("./routes/user"); //make sure it's in the current directory

app.use("/api/users", userRoutes);

app.listen(process.env.PORT || 4000, () => {
  console.log(`App is running on ${process.env.PORT || 4000} `);
});
