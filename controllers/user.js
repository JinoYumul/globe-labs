const User = require("../models/User");

module.exports.register = (params) => {
  let user = new User({
    firstname: params.firstname,
    lastname: params.lastname,
    email: params.email,
    mobileNumber: params.mobileNumber,
    password: params.password,
    loginType: "email",
  });

  return user.save().then((user, err) => {
    return err ? false : true;
  });
};
